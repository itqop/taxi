using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGarage : MonoBehaviour
{
    public SaveSysten _save;

    private void Start()
    {
        _save = GameObject.Find("GameManager").GetComponent<SaveSysten>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Car")
        {
            _save.saveGarage();
            SceneManager.LoadScene(2);
        }
    }
}
