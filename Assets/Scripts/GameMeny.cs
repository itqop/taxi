using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMeny : MonoBehaviour
{
    public Button _contB;

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetInt("FirstSave") > 0)
        {
            _contB.interactable = true;
        }

        if (PlayerPrefs.GetInt("FirstSave") == 0)
        {
            _contB.interactable = false;
        }
    }

    public void exit()
    {
        Application.Quit();
    }
    public void NewGame()
    {
        PlayerPrefs.SetInt("FirstSave",0);
        SceneManager.LoadScene(1);
    }
    public void Contin()
    {
        SceneManager.LoadScene(1);
    }
}
