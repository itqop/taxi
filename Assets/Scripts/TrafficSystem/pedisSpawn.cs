using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class pedisSpawn : MonoBehaviour
{
    public GameObject pedPref;
    public int pedestriansToSpawn;

    private void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        int count = 0;

        while (count < pedestriansToSpawn)
        {
            GameObject obj = Instantiate(pedPref);
            if (pedPref.gameObject.name == "TestTS")
            {
                obj.name = "pedestrian";
                Transform child = transform.GetChild(Random.Range(0, transform.childCount - 1));
                obj.GetComponent<NavWaypoint>().currentWayPoint = child.GetComponent<WayPoint>();
                obj.transform.position = child.position;
            }
                
            if (pedPref.gameObject.name == "CarAi")
            {
                obj.name = "CarAi";
                Transform child = transform.GetChild(Random.Range(0, transform.childCount - 1));
                obj.GetComponent<CarControllerAI>().currentWayPoint = child.GetComponent<WayPoint>();
                obj.transform.position = child.position;
            }
                
            

            yield return new WaitForEndOfFrame();

            count++;
        }
    }
}
