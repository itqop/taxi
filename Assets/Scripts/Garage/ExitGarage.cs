using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitGarage : MonoBehaviour
{
    public ChoiceBtn _choiceBtn;
    private void Start()
    {
        _choiceBtn = GetComponent<ChoiceBtn>();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Exit()
    {
        PlayerPrefs.SetInt("CurrentCar",_choiceBtn.CurrentPos);
        SceneManager.LoadScene(1);
    }
    
}
